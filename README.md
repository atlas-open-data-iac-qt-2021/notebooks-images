# ATLAS Open Data ROOT notebook

This is a set of [Jupyter notebooks](https://jupyter.org/) images, whose purpose is to provide a working environment for the ATLAS Open Data project.

These notebook embed the features of a [scipy notebook](https://jupyter-docker-stacks.readthedocs.io/en/latest/using/selecting.html#jupyter-scipy-notebook) and the [ROOT](https://root.cern/) framework used at CERN.
